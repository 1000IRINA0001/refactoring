package refactoring;


import java.util.Arrays;
import java.util.Scanner;


public class SearchNumber {
    private static final int MIN = -25;
    private final static int RANGE = 51;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Введите размер массива: ");
        int arraySize = enterLengthArray();
        int[] array = new int[arraySize];

        array=randomlyFillTheArray(arraySize, MIN, RANGE);

        System.out.println("Введите число, которое хотите найти : ");
        int numberEntered = scanner.nextInt();

        int result = searchNumber(array, numberEntered);
        if (result == -1) {
            System.out.println("Такого числа в масиве не найдено ");
        } else {
            System.out.println("число в массиве найдено, его индекс: " + result);
        }
    }
    /**
     * метод заполняет массив рандомными числами (MIN - минимальное число в массиве, RANGE - диапазон чисел в массиве)
     *
     * @return возращает заполненный массив
     */
    private static int[] randomlyFillTheArray(int arraySize, int MIN, int RANGE) {
        int[] array = new int[arraySize];
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * RANGE);
        }
        System.out.println(Arrays.toString(array));
        return array;
    }

    /**
     * метод в массиве последовательно ищет число, введенное пользователем
     *
     * @param numberEntered введенное пользователем число
     * @return возращает индекс элемента в последовательности или 0, если такого элемента нет
     */
    private static int searchNumber(int[] array, int numberEntered) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numberEntered) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return -1;
        } else {
            return index;
        }
    }

    /**
     * метод проверяет введенный с консоли размер массива
     *
     * @return возращает размер массива
     */
    private static int enterLengthArray() {
        int size;
        do {
            size = scanner.nextInt();
        } while (size < 1);
        return size;
}}

